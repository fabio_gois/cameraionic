import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-preview',
  templateUrl: 'preview.html',
})
export class PreviewPage {

  picture: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let photo = navParams.get('photo');
    this.picture = photo;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreviewPage');
  }

}
