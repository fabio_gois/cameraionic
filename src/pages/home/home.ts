import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import Cropper from 'cropperjs';

import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  picture: any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private loadingController: LoadingController,
    private cameraPreview: CameraPreview
    ) {

      // this.cameraPreview.setPreviewSize({width: 250, height: 250});

    }


  ionViewDidLoad(){
    console.log("didLoad");
  }

  startCamera() {
    console.log("startCamera called");
    const options: CameraPreviewOptions = { 
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.width, 
      tapPhoto: true,
      previewDrag: true,
      toBack: false,
      alpha: 1
    };
    this.cameraPreview.startCamera(options).then((res) => {
      console.log(res)
      console.log("startCamera res: " + res);
    },
    (err) => {
      console.log("ERROR previewCamera: " + err);
    });
   
  }
  takePicture() {
    let loader = this.loadingController.create({
        content: "Aguarde"
      });
    loader.present();

    console.log("takepicture");

    const pictureOpts: CameraPreviewPictureOptions = {
      width: 250,
      height: 250,
      quality: 100
    }

    this.cameraPreview.takePicture(pictureOpts).then((imageData) => {

      var getPicture = 'data:image/jpeg;base64,' + imageData;


        // var getPictureOuput = new Cropper(imageData, { aspectRatio: 1 / 1 });


        // let croppedImg = getPictureOuput.getCroppedCanvas({ width: 500, height: 500}).toDataURL('image/jpeg');
        // let croppedImg = this.cropperInstance.getCroppedCanvas({ width: 500, height: 500}).toDataURL('image/jpeg');

        // var getPicture = 'data:image/jpeg;base64,' + croppedImg;

        // var ctx: CanvasRenderingContext2D;
        // var offsetX = 0.5;
        // var offsetY = 0.5;
        // var getPictureOutput = this.drawImageProp(ctx, getPicture, 0, 0, 250, 250, offsetX, offsetY);
        // this.imageElement.nativeElement.src = imageData;

        this.picture = getPicture;
        this.cameraPreview.stopCamera();
        // this.cameraPreview.hide();
        this.navCtrl.push('PreviewPage', {
          photo: this.picture
        }).then((success) => {
          loader.dismiss();
        })
      }, (err) => {
        console.log("takePicture error: " + err);
        loader.dismiss();
      });
  }

  

  cropDone() {
    // let croppedImg = this.cropperInstance.getCroppedCanvas({ width: 500, height: 500}).toDataURL('image/jpeg');

    // do whatever you want with base64 variable croppedImg
  }





  tapFocus(event) {
    var xPos = event.changedTouches[0].pageX;
    var yPos = event.changedTouches[0].pageY;
    console.log('X: ' + xPos + ', Y: ' + yPos);
    this.cameraPreview.tapToFocus(xPos, yPos)
  }
  switchCamera() {
    this.cameraPreview.switchCamera();
  }

  colorEffect() {
    this.cameraPreview.setColorEffect("negative");
  }


drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {
  if (arguments.length === 2) {
      x = y = 0;
      w = ctx.canvas.width;
      h = ctx.canvas.height;
  }

  console.log(offsetX);
  console.log(offsetY);

  /// default offset is center
  offsetX = typeof offsetX === 'number' ? offsetX : 0.5;
  offsetY = typeof offsetY === 'number' ? offsetY : 0.5;

  /// keep bounds [0.0, 1.0]
  if (offsetX < 0) offsetX = 0;
  if (offsetY < 0) offsetY = 0;
  if (offsetX > 1) offsetX = 1;
  if (offsetY > 1) offsetY = 1;

  var iw = img.width,
      ih = img.height,
      r = Math.min(w / iw, h / ih),
      nw = iw * r,   /// new prop. width
      nh = ih * r,   /// new prop. height
      cx, cy, cw, ch, ar = 1;

  /// decide which gap to fill    
  if (nw < w) ar = w / nw;
  if (nh < h) ar = h / nh;
  nw *= ar;
  nh *= ar;

  /// calc source rectangle
  cw = iw / (nw / w);
  ch = ih / (nh / h);

  cx = (iw - cw) * offsetX;
  cy = (ih - ch) * offsetY;

  /// make sure source rectangle is valid
  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;

  /// fill image in dest. rectangle
  return ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
}

  


}
